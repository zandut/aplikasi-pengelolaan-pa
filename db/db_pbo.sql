-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 02. Januari 2013 jam 17:37
-- Versi Server: 5.5.8
-- Versi PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pbo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(6) NOT NULL,
  `password` varchar(6) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`username`, `password`, `email`) VALUES
('admin', '4dmin', 'admin@yahoo.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE IF NOT EXISTS `fakultas` (
  `id_fakultas` varchar(6) NOT NULL,
  `nama_fakultas` varchar(40) NOT NULL,
  `gedung` varchar(40) NOT NULL,
  PRIMARY KEY (`id_fakultas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `nama_fakultas`, `gedung`) VALUES
('FAK-01', 'INFORMATIKA', 'GEDUNG F'),
('FAK-02', 'TELEKOMUNIKASI', 'GEDUNG FEK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nim` varchar(9) NOT NULL,
  `nama_mahasiswa` varchar(40) NOT NULL,
  `tahun` int(11) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  `no_telp` varchar(10) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `nip` varchar(6) NOT NULL,
  `id_fakultas` varchar(6) NOT NULL,
  PRIMARY KEY (`nim`),
  KEY `nip` (`nip`,`id_fakultas`),
  KEY `id_fakultas` (`id_fakultas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama_mahasiswa`, `tahun`, `no_hp`, `no_telp`, `alamat`, `password`, `tgl_lahir`, `email`, `nip`, `id_fakultas`) VALUES
('11213445', 'saddam', 2012, '0897567897', '', 'pga', 'sapiijo', '1992-09-14', 'dsaasd@rere.com', '09000', 'FAK-01'),
('113118024', 'bunz', 2005, '085722675501', '', 'putra bangsa', '800442', '1989-01-26', 'nashiruddin.abubakar@gmail.com', '123', 'FAK-01'),
('114120013', 'Rumor', 2007, '0871237123', '', 'Sukabirus', 'rumor123', '1993-01-13', 'rumor@yahoo.com', '123', 'FAK-02'),
('613110035', 'Muhammad Fauzan', 2011, '087735042317', '', 'Bandung', 'ravenski', '1993-09-15', 'fauzan_blackangels20@yahoo.co.id', 'PEM-05', 'FAK-01'),
('613120035', 'Rudy', 2009, '0877123123', '', 'Sukabirus', '123456', '1993-01-07', 'rudy@yahoo.com', '0980-1', 'FAK-01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pa`
--

CREATE TABLE IF NOT EXISTS `pa` (
  `ID_PA` varchar(7) NOT NULL,
  `judul_pa` varchar(100) NOT NULL,
  `abstraksi` varchar(300) NOT NULL,
  `nim` varchar(9) NOT NULL,
  `url` varchar(100) NOT NULL,
  PRIMARY KEY (`ID_PA`),
  UNIQUE KEY `nim` (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pa`
--

INSERT INTO `pa` (`ID_PA`, `judul_pa`, `abstraksi`, `nim`, `url`) VALUES
('PA-1', 'Aplikasi Lucu', 'Aplikasi apa itu ?', '613110035', '../Upload/IF/613110035/613110035.pdf'),
('PA-2', 'asdasd', 'asdasd', '11213445', '../Upload/IF/11213445/11213445.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembimbing`
--

CREATE TABLE IF NOT EXISTS `pembimbing` (
  `nip` varchar(6) NOT NULL,
  `nama_pembimbing` varchar(40) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembimbing`
--

INSERT INTO `pembimbing` (`nip`, `nama_pembimbing`) VALUES
('09000', 'Emma'),
('0980-1', 'Untari'),
('123', 'admin'),
('3123', '12312 (belum valid)'),
('PEM-01', 'Retno Novi'),
('PEM-02', 'Tjokorda'),
('PEM-03', 'Mahmud Imrona'),
('PEM-04', 'Mahmud Dwi'),
('PEM-05', 'Hetti Hidayati'),
('PEM-06', 'Irawan Thamrin'),
('PEM-07', 'Leonardi'),
('PEM-08', 'Bayu Munajat'),
('Pem-09', 'Kurniawan'),
('pem-10', 'Tri Brotoharsono'),
('pem-11', 'Giva'),
('pem-15', 'Dawam'),
('wer', 'qwr');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `pembimbing` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id_fakultas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pa`
--
ALTER TABLE `pa`
  ADD CONSTRAINT `pa_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`) ON DELETE CASCADE ON UPDATE CASCADE;
