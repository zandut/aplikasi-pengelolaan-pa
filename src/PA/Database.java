/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PA;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author FANSKY
 */
public class Database {
    private String dbUser="root";
    private String dbPass="";
    private Statement sta=null;
    private Connection con=null;
    private ResultSet rs=null;
    public boolean isInsert = false;
    public Database()
    {
        try
        {
            Class.forName("org.gjt.mm.mysql.Driver");
        }
        catch (Throwable f)
        {
            JOptionPane.showMessageDialog(null, ""+f.getMessage(), "JDBC Driver Error ", JOptionPane.WARNING_MESSAGE);
        }
        
        try
        {
            con = DriverManager.getConnection("jdbc:mysql://localhost/db_pbo", dbUser, dbPass);
            sta = (Statement) con.createStatement();
        }
        catch (Throwable f)
        {
            JOptionPane.showMessageDialog(null, ""+f.getMessage(), "Connection Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public ResultSet getResult(String sql)
    {
        try
        {
            rs = sta.executeQuery(sql);
        }
        catch (Throwable f)
        {
            JOptionPane.showMessageDialog(null, ""+f.getMessage(), "Communication Error", JOptionPane.ERROR_MESSAGE);
        }
        return rs;
    }
    
    public void query(String sql)
    {
        try
        {
            sta.executeUpdate(sql);
           
            isInsert = true;
        }
        catch (Throwable f)
        {
            isInsert = false;
            JOptionPane.showMessageDialog(null, "Terjadi kesalahan dalam database\n"+f.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
   
    
}
